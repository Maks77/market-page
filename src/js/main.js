$(document).ready(function () {
	


	$('.account-link').on('click', function (e) {
		e.preventDefault();
		$('.drop-field.account-drop').toggleClass('x-open')
	})

	$('.phone-link').on('click', function (e) {
		e.preventDefault();
		$('.drop-field.call').toggleClass('x-open')
	})

	$('.burg-left').on('click', function () {
		$('.top-line-wrapper').toggleClass('x-open')
	})

	$('.product-trigger').on('click', function () {
		$('.product-holder').toggleClass('x-open')
	})

	$('.category-list-item').on('click', function () {
		$('.sublist').toggleClass('x-open')
	})


	//======= filters ==============================================

	$('.filter__title').on('click', function () {
		// console.log('click')
		let parent = $(this).parent('.filter')
		// console.log(parent)
		$(parent).children('.filter__drop-block').toggleClass('x-open')
	})

	$('.filter-list-drop-btn').on('click', function () {
		$('.filter-list').toggleClass('filter-list-open')
	})
	//======= filters ==============================================

	$(document).mouseup(function (e){
		//.x-btn - кнопка выпада окна
		//.x-open - класс открытия модального
		//.x-close-btn - крестик закрытия внутри модального окна
		//.x-drop - класс модального окна

		let win = $('.x-drop.x-open');

		if (win.length > 0 ) {

			if ( !win.is(e.target) ) {
				if ( $(e.target).closest('.x-close-btn').length !== 0 || win.has(e.target).length === 0) {
					// console.log('not on child');
					
					if ($(e.target).closest('.x-btn').length !== 0) return;
					win.removeClass('x-open');
				}
			}
		}

	})


	$('[data-for]').on('click', function () {
		if ( $(this).hasClass('active') ) return;

		let target = $(this).data().for;

		$('[data-for]').removeClass('active');
		$('._z-modal').removeClass('active');
		$(this).addClass('active');
		$(target).addClass('active')
	})

	$('.star-rating input').on('change', function (e) {
		let target = e.target;
		let parent = $(e.target).parents('label')
		if (target.checked) {
			// console.log('log')
			// console.log($(parent::before))
			$(parent).addClass('mod')
		}
	})


	$('.brands ul').slick({
		centerMode: true,
		centerPadding: '60px',
		slidesToShow: 6,
		prevArrow: '<div class="btn-arrow btn-left"><i class="fa fa-chevron-left"></i></div>',
		nextArrow: '<div class="btn-arrow btn-right"><i class="fa fa-chevron-right"></i></div>',
		responsive: [
		{
			breakpoint: 768,
			settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 3
			}
		},
		{
			breakpoint: 480,
			settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 1
			}
		}
		]
	});

	$('.product-block_item-slider-wrapper').slick({
		infinite: true,
		draggable: true,
		focusOnSelect: true,
		slidesToShow: 4,
		swipeToSlide: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					centerMode: false,
					centerPadding: '40px',
					slidesToShow: 3
				}
			},
			{
				breakpoint: 480,
				settings: {
					variableWidth: false,
					arrows: false,
					centerMode: true,
					centerPadding: '60px',
					slidesToShow: 2
				}
			},
			{
				breakpoint: 390,
				settings: {
					variableWidth: false,
					arrows: false,
					centerMode: true,
					centerPadding: '80px',
					slidesToShow: 1
				}
			}
		]
	});



	$('.zoom-btn').on('mouseup', function (e) {
		console.log('zoom');
		let imgBlock = $(this).siblings("img").clone();
		// let parent = $(this).parent().clone();
		$('.popup').addClass('open');
		$('.popup .inner-holder').html(imgBlock);
		console.dir(parent);
	})

	$('.popup-close').on('mouseup', function () {
		$('.popup .inner-holder').empty();
		$('.popup').removeClass('open');
	})

	$('.product-block_item-img').on('mouseup', function () {
		$('.product-block_item-img').find('img').removeClass('active');
		let img = $(this).find('img');
		console.log(img);
		img.addClass('active')
		$('.specific-drop').addClass('show');
	})
	
	
	
	let minPrice = 1000;
	let maxPrice = 4500;

	let minV = $('.min-price').val(minPrice + ' грн.');
	let maxV = $('.max-price').val(maxPrice + ' грн.');
	
	if ( $('div').is('#slider')) {
		$( "#slider" ).slider({
			range: true,
			values: [ minPrice, maxPrice ],
			min: 0,
			max: 10000,
			slide: function (event, ui) {
				// console.log('event', event)
				// console.log('ui', ui)
				$(minV).val(ui.values[0] + ' грн.')
				$(maxV).val(ui.values[1] + ' грн.')
			}
		});
	}

	$('.delivery-item').on('click', function () {
		if ( $(this).find('.delivery-item__drop-content').hasClass('open') ) {
			$(this).find('.delivery-item__drop-content').removeClass('open');
			return;
		} else {

		}
		$('.delivery-item__drop-content').removeClass('open');
		$(this).find('.delivery-item__drop-content').toggleClass('open')
		// $('.delivery-item__drop-content').removeClass('open');
	})
	


	// orderplace_personal-info
	// orderplace_delivery-vars

	// orderplace_chosen-products

	// orderplace-accept

	let state = {
		first: '.orderplace_personal-info',
		second: '.orderplace_delivery-vars',
		third: '.orderplace-accept',
		chosenProd: '.orderplace_chosen-products'
	}


	$('.orderplace-bookmark').on('mouseup', function () {
		let data = $(this).data().target;
		$(this).addClass('active checked')
		

		switch (data) {
			case 'first':
				$(state.first).removeClass('hidden');
				$(state.chosenProd).removeClass('hidden');

				$(state.second).addClass('hidden');
				$(state.third).addClass('hidden');
				break;

			case 'second':
				$(state.second).removeClass('hidden');
				$(state.chosenProd).removeClass('hidden');

				$(state.first).addClass('hidden');
				$(state.third).addClass('hidden');
				break;

			case 'third':
				$(state.third).removeClass('hidden');

				$(state.chosenProd).addClass('hidden');
				$(state.first).addClass('hidden');
				$(state.second).addClass('hidden');
				break;
		}
	})


	
})

