var gulp = require('gulp'),
		watch = require('gulp-watch'),
		prefixer = require('gulp-autoprefixer'),
		uglify = require('gulp-uglify'),
		//sass = require('gulp-sass'),
		less = require('gulp-less'),
		sourcemaps = require('gulp-sourcemaps'),
		rigger = require('gulp-rigger'),
		cssmin = require('gulp-minify-css'),
		imagemin = require('gulp-imagemin'),
		pngquant = require('imagemin-pngquant'),
		rimraf = require('rimraf'),
		browserSync = require('browser-sync'),
		minifyCss = require('gulp-minify-css'),
		rename = require('gulp-rename'),
		webpack = require('webpack'),
		webpackStream = require('webpack-stream'),
		gcmq = require('gulp-group-css-media-queries'),
		reload = browserSync.reload;

var path = {
	build: {
		html: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/img',
		fonts: 'build/fonts/',
		libs: 'build/libs'
	},

	src: {
		html: 'src/*.html',
		// js: 'src/js/main.js',
		js: 'src/js/**/*.js',
		style: 'src/style/main.less', //scss
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*',
		libs: 'src/libs/**/*.*'
	},

	watch: {
		html: 'src/**/*.html',
		js: 'src/js/**/*.js',
		style: 'src/style/**/*.less', //scss
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*',
		libs: 'src/libs/**/*.*'
	},

	clean: './build/'
};

var config = {
	server: {
		baseDir: './build/',
		// index: 'touch-page.html',
		serveStaticOptions: {
	    	extensions: ["html"]
	    }
		// index: 
			// 'index.html',
			// 'category.html',
			// 'product-item.html',
			// 'contacts.html',
			// 'pay-and-delivery.html',
			// 'orderplace.html'
	},
	notify: false,
	tunnel: true,
	host: 'localhost',
	port: 9000,
	logPrefix: 'Frontend_Devil'
};

gulp.task('html:build', function () {
	gulp.src(path.src.html)
		.pipe(rigger())
		.pipe(gulp.dest(path.build.html))
		.pipe(reload({stream: true}));
});

// gulp.task('js:build', function () {
// 	gulp.src(path.src.js)
// 		// .pipe(rigger())
// 		// .pipe(sourcemaps.init())
// 		// .pipe(uglify())
// 		// .pipe(sourcemaps.write())
// 		.pipe(gulp.dest(path.build.js))
// 		.pipe(reload({stream: true}));
// 	console.log('------------------------------------------js done')
// });

gulp.task('js:build', function () {
  return gulp.src('./src/js/main.js')
    .pipe(webpackStream({
      output: {
        filename: 'main.js',
      },
      mode: 'development',
      resolve: {
      	modules: ['node_modules']
      },
      module: {
        rules: [
          {
            test: /\.(js)$/,
            exclude: /(node_modules)/,
            loader: 'babel-loader',
            query: {
              presets: ['env']
            }
          }
        ]
      },
      externals: {
        //jquery: 'jQuery'
      }
    }))
    .on('error', function handleError() {
		this.emit('end'); // Recover from errors
	})
    .pipe(gulp.dest('./build/js'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./build/js/'))
    .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
	gulp.src(path.src.style)
		.pipe(sourcemaps.init())
		//.pipe(sass())
		.pipe(less())
		// .pipe(minifyCss())
		.pipe(prefixer({
			browsers: ['last 8 versions']
		}))
		// .pipe(cssmin())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
	gulp.src(path.src.img)
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()],
			interlaced: true
		}))
		.pipe(gulp.dest(path.build.img))
		.pipe(reload({stream: true}));
});

gulp.task('fonts:build', function () {
	gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
});

gulp.task('libs:build', function () {
	gulp.src(path.src.libs)
		.pipe(gulp.dest(path.build.libs))
});

gulp.task('build', [
	'html:build',
	'js:build',
	'style:build',
	'libs:build',
	'fonts:build',
	'image:build',
]);

gulp.task('watch', function(){
	watch([path.watch.html], function(event, cb) {
		gulp.start('html:build');
	});
	watch([path.watch.style], function(event, cb) {
		gulp.start('style:build');
	});
	watch([path.watch.js], function(event, cb) {
		gulp.start('js:build');
	});
	watch([path.watch.img], function(event, cb) {
		gulp.start('image:build');
	});
	watch([path.watch.fonts], function(event, cb) {
		gulp.start('fonts:build');
	});
	watch([path.watch.libs], function(event, cb) {
		gulp.start('libs:build');
	});
});


gulp.task('webserver', function () {
	browserSync(config);
});

gulp.task('clean', function (cb) {
	rimraf(path.clean, cb);
});

gulp.task('default', [
	'build',
	'webserver',
	'watch'
]);